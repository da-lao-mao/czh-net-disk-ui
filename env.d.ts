/// <reference types="vite/client" />
/**
 * new config.
 */


declare type FItem = {
    fileName:string,
    folderFlag:boolean,
    createTime:string
}

type UploadFile = {
    name: string,
    speed: string,
    suspend: boolean,
    totalSlice: number,
    uploadedSlice: number,
    lock: Function,
    unlock: Function,
    abort: boolean,
    progress: number,
    abortController: AbortController
}

////////////////////////////////////////////////
//com.example.demo.common.R
declare type ResponseData = {
    code:string,
    message:string,
    show:boolean,
    value:any
}
com.example.demo.common.CurrentUser
declare type UserDetail = {
    id:string
    username: string,
    email:string
}
//com.example.demo.common.CurrentUserVo
declare type CurrentUser = {
    token:string,
    detail:UserDetail
}

declare type DaoLoginUser = {
    username: string
    password: string
}
declare type MailLoginUser = {
    email: string
    code: string
}