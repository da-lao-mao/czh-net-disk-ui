import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

// import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import App from './App.vue'
import router from './router'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
const pinia = createPinia()
pinia.use((context) => {
    if(context.store.$id == 'currentUser') {
        const token = localStorage.getItem('token')
        if(token) {
            context.store.setValue(JSON.parse(token))
        }
    }
})


for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }

app.use(pinia)
app.use(router)
// app.use(ElementPlus)
app.mount('#app')
