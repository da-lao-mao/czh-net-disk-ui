import type { AxiosProgressEvent } from "axios";
import server from "..";

const PREFIX = 'file/'



class _File<T> {
    resume(fileName: string, path: string) {
        return server.get(PREFIX + `resume?fileName=${fileName}&path=${path}`)
    }
    del(fileName: string, path: string) {
        return server.get(PREFIX + `del?fileName=${fileName}&path=${path}`)
    }
    abort(sliceFileName: string,uploadedSlice:number) {
        return server.get<T>(PREFIX + `abort?sliceFileName=${sliceFileName}&uploadedSlice=${uploadedSlice}`)
    }

    upload(file:any,path:string,monitor:(e:AxiosProgressEvent)=>void | any= (e) => {},abortController:AbortController) {
        return server.post<T>(PREFIX+'upload',{
            file,
            path
        },{
            headers:{
                'Content-Type': 'multipart/form-data'
            },
            onUploadProgress:monitor,
            signal:abortController?.signal
        }
    )
    }
    uploadSlice(blob:any,sliceFileName:string,abortController:AbortController) {
        return server.post<T>(PREFIX+'uploadSlice',{
            blob,
            sliceFileName
        },{
            headers:{
                'Content-Type': 'multipart/form-data'
            },
            signal:abortController.signal
        })
    }

    coalesce(path:string, sliceFileName:string,fileName:string,sliceTotalNumber:number) {
        return server.get<T>(PREFIX + `coalesce?path=${path}&sliceFileName=${sliceFileName}&sliceTotalNumber=${sliceTotalNumber}&fileName=${fileName}`)
    }

    getCurrentItems(path:string) {
        return server.get<T>(PREFIX + `getCurrentItems?path=${path}`)
    }
    mkdir(path:string,dirName:string) {
        return server.get(PREFIX + `mkdir?path=${path}&dirName=${dirName}`)
    }

    rename(oldName:string,newName:string,path:string) {
        return server.get(PREFIX + `rename?oldName=${oldName}&newName=${newName}&path=${path}`)
    }
    mv(from:string,to:string,path:string) {
        return server.get(PREFIX + `mv?from=${from}&to=${to}&path=${path}`)
    }
}

export default new _File<ResponseData>()